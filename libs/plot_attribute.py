from datetime import datetime
from typing import Literal
from typing_extensions import final

@final
class PlotAttributes:
    def __init__(
        self,
        input_paths_and_labels: list[tuple[str, str | None]],
        output_path: str,
        start_time: datetime,
        end_time: datetime,
        title: str | None,
        x_label: str | Literal[""] | None,
        y_label: str | Literal[""] | None,
        raw_incident: bool,
        add_incidents: list[tuple[datetime, str]],
        dpi: int,
        ratio: float
    ) -> None:
        self.input_paths_and_labels = input_paths_and_labels
        self.output_path = output_path
        self.start_time = start_time
        self.end_time = end_time
        self.title = title
        self.x_label = x_label
        self.y_label = y_label
        self.raw_incident = raw_incident
        self.add_incidents = add_incidents
        self.dpi = dpi
        self.ratio = ratio
        pass
