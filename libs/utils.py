from datetime import datetime, date, time
import os, sys


def parse_time(s: str) -> datetime:
    try:
        dt = datetime.combine(
            date.today(),
            time.fromisoformat(s)
        )
    except Exception:
        print(
            """--add-incident doesn't get valid datetime format!
        Run `python -c 'from datetime import time;help(time.isoformat)'`
        for more information."""
        )
        sys.exit(1)
    return dt
