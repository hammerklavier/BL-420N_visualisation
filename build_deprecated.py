__version__ = "0.0.4"
__project__ = "BL-420N_visualisation"
__author__ = "hammerklavier@noreply.gitcode.com"
__email__ = "q5vsx3@163.com"
__copyright__ = "Copyright (c) 2025 hammerklavier@noreply.gitcode.com"
__license__ = "GNU GENERAL PUBLIC LICENSE Version 3"
__credits__ = ["RIBBON"]
__status__ = "Prototype"
__issue__ = "https://gitcode.com/hammerklavier/BL-420N_visualisation/issues"

import argparse
import os, sys, shutil

description = f"""Build script v{__version__} for {__project__},
by {__author__}.
This project follows {__license__}.
If you find any issues, please report them to {__issue__}.
"""

parser = argparse.ArgumentParser(description=description)

group = parser.add_argument_group("Choose your wrapper")
wrapper_group = group.add_mutually_exclusive_group(required=True)
wrapper_group.add_argument(
    "--pyinstaller", "-p",
    action="store_true",
    help="Build with PyInstaller."
)
wrapper_group.add_argument(
    "--nuitka", "-n",
    action="store_true",
    help="Build with Nuitka."
)

file_or_dir_group = parser.add_argument_group("Create a folder or an executable. Currently it only works for pyinstaller.")
exclusive_FoD_group = file_or_dir_group.add_mutually_exclusive_group()
exclusive_FoD_group.add_argument(
    "--one-file", "-F", action="store_true",
    help="Create single executable."
)
exclusive_FoD_group.add_argument(
    "--one-dir", "-D", action="store_true",
    help="Create one dir with an executable inside."
)

if __name__ == "__main__":
    args = parser.parse_args()
    if args.pyinstaller:
        print("PyInstaller mode.")
        code = os.system("pip install pyinstaller")
        if code != 0:
            print("Failed to install PyInstaller!")
            sys.exit(code)
        # clean cache
        if os.path.exists("build"):
            shutil.rmtree("build")
        # build
        if args.one_dir:
            code = os.system("pyinstaller -D -p . --add-data ./fonts/*:./fonts BL_420N_visualisation.py")
        else:
            code = os.system("pyinstaller -F -p . --add-data ./fonts/*:./fonts BL_420N_visualisation.py")
        if code == 0:
            print("Build success. Single executable/folder is in dist folder.")
        else:
            print("Build failed.")
            sys.exit(code)
    elif args.nuitka:
        print("Nuitka mode.")
        # Windows system
        if sys.platform == "win32":
            # check if MSVC is installed
            if os.system("where.exe cl.exe") == 0:
                print("MSVC detected.")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --msvc=latest --lto=yes --enable-plugins=pyside6 BL_420N_visualisation.py")
                if code == 0:
                    print("Build success. Single executable is in current folder.")
                else:
                    print("Build failed.")
                    sys.exit(code)
            else:
                print("MSVC is not found. Build with mingw64.")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --lto=yes --enable-plugins=pyside6 BL_420N_visualisation.py")

            pass
        # Linux system
        elif sys.platform == "linux":
            if os.system("which clang") == 0:
                print("Clang detected.")
                # code = os.system("nuitka --standalone --onefile --clang --lto=yes --enable-plugins=pyside6 BL_420N_visualisation.py")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --clang --lto=yes --enable-plugins=pyside6 BL_420N_visualisation.py")
            elif os.system("which gcc") == 0:
                print("GCC detected.")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --lto=yes --enable-plugins=pyside6 BL_420N_visualisation.py")
            else:
                print("GCC or Clang is not found. Try nuitka's default choice")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --enable-plugins=pyside6 BL_420N_visualisation.py")
                if code == 0:
                    print("Build success. Single executable is in current folder.")
                    sys.exit(0)
                else:
                    print("Build failed.")
                    sys.exit(code)
            if code == 0:
                print("Build success. Single executable is in current folder.")
                sys.exit(0)
            else:
                print("Build failed. Try nuitka's default choice")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --enable-plugins=pyside6 BL_420N_visualisation.py")
                if code == 0:
                    print("Build success. Single executable is in current folder.")
                    sys.exit(0)
                else:
                    print("Build failed.")
                    sys.exit(code)
        # MacOS X system
        elif sys.platform == "darwin":
            if os.system("which clang") == 0:
                print("Clang detected.")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --clang --lto=yes --enable-plugins=pyside6 BL_420N_visualisation.py")
            elif os.system("which gcc") == 0:
                print("GCC detected.")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --lto=yes --enable-plugins=pyside6 BL_420N_visualisation.py")
            if code == 0:
                print("Build success. Single executable is in current folder.")
                sys.exit(0)
            else:
                print("Build failed. Try nuitka's default choice")
                code = os.system("nuitka --standalone --onefile --include-data-dir=./fonts=fonts --include-data-files=./gui/main.py=./gui/main.py --enable-plugins=pyside6 BL_420N_visualisation.py")
                if code == 0:
                    print("Build success. Single executable is in current folder.")
                    sys.exit(0)
