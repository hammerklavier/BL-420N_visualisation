import os

if os.name == "posix":
    os.system(
        "pip install numpy \
        seaborn \
        matplotlib \
        polars[pyarrow] \
        aiofiles \
        pyside6"
    )
elif os.name == "nt":
    os.system(
        "pip install -r requirements.txt"
    )
