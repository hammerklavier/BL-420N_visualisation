import argparse
import sys

__version__ = "2.0.0-rc4"

parser = argparse.ArgumentParser(description="Plot BL_420N data.")

parser.add_argument("--no-gui", action="store_true",
                    help="Plot data without GUI.")

io_group = parser.add_argument_group("io designation")
io_group.add_argument("--input", "-i", nargs="+",
                        help="Input CSV file")
io_group.add_argument(
    "--output", "-o", help="Path to save result")

time_group = parser.add_argument_group("Time range declaration")
time_group.add_argument("--start", "-s", help="Start time to plot")
time_group.add_argument("--end", "-e", help="End time to plot")

figure_argument = parser.add_argument_group("Figure attribute setting")
figure_argument.add_argument("--title", "-t", help="Title of figure")
figure_argument.add_argument("--xlabel", help="X-axis label")
figure_argument.add_argument("--ylabel", help="Y-axis label")
figure_argument.add_argument(
    "--label", "-l", nargs="+", help="Legend of figure. Number of which should match that of input files")

figure_argument.add_argument(
    "--incident", help="plot incident line", action="store_true")
figure_argument.add_argument("--add-incident", nargs="+",
                    help="Draw a/multiple verticle line(s) at certain time(s), indicating an/multiple incident(s). Format is [(HH:MM:SS, description)]")
figure_argument.add_argument("--dpi", help="DPI of figure", default=300)

ylim_group = parser.add_argument_group("Y limit Setting")
ylim_group_exclusive = ylim_group.add_mutually_exclusive_group()
ylim_group_exclusive.add_argument(
    "--y-quantile-range", nargs=2, default=[0.05, 0.95],
    help="Set y-limit according to the maximum of quantile of each set of data."
)
ylim_group_exclusive.add_argument(
    "--y-value-range", nargs=2,
    help="Set y-limit according to your assigned bound value."
)

parser.add_argument("--verbose", '-v',
                    action="store_true", help="Verbose mode")


if __name__ == "__main__":
    args = parser.parse_args()
    if args.no_gui:
        print("No GUI mode.")

        if args.input is None:
            parser.error("--no-gui mode requires --input/-i argument!")
            sys.exit(1)
        elif args.start is None or args.end is None:
            parser.error("--start and --end parameter should be offered!")

        from no_gui.plot_data import no_gui_plot

        no_gui_plot(parser)
        pass
    else:
        print("GUI mode.")
        from PySide6.QtWidgets import QApplication
        from gui.main import MyWidget

        app = QApplication([])
        my_widget = MyWidget()
        my_widget.show()

        sys.exit(app.exec())
