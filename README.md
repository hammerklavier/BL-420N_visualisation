# BL-420N_visualisation v2.0

可视化 BL-420N 生物信号采集与分析系统的原始数据文件。

其他相似产品的原始数据导出文件未进行测试。

## 用法

运行前，请先参考 [环境创建](#环境创建)

### 参数

#### GUI

通过在 powershell 中输入 `python ./BL_420N_visualisation.py` 运行 GUI 客户端，或双击发行版的 `BL_420N_visualisation.exe` 可执行文件。

#### No GUI

通过在 powershell 中输入 `python ./BL_420N_visualisation.py -h` 获取帮助。

```text
usage: BL_420N_visualisation.py [-h] [--no-gui] [--input INPUT [INPUT ...]] [--output OUTPUT] [--start START] [--end END]
                                [--title TITLE] [--xlabel XLABEL] [--ylabel YLABEL] [--label LABEL [LABEL ...]] [--incident]
                                [--add-incident ADD_INCIDENT [ADD_INCIDENT ...]] [--dpi DPI]
                                [--y-quantile-range Y_QUANTILE_RANGE Y_QUANTILE_RANGE | --y-value-range Y_VALUE_RANGE Y_VALUE_RANGE]
                                [--verbose]

Plot BL_420N data.

options:
  -h, --help            show this help message and exit
  --no-gui              Plot data without GUI.
  --verbose, -v         Verbose mode

io designation:
  --input INPUT [INPUT ...], -i INPUT [INPUT ...]
                        Input CSV file
  --output OUTPUT, -o OUTPUT
                        Path to save result

Time range declaration:
  --start START, -s START
                        Start time to plot
  --end END, -e END     End time to plot

Figure attribute setting:
  --title TITLE, -t TITLE
                        [(HH:MM:SS, description)]
  --dpi DPI             DPI of figure

Y limit Setting:
  --y-quantile-range Y_QUANTILE_RANGE Y_QUANTILE_RANGE
                        Set y-limit according to the maximum of quantile of each set of data.
  --y-value-range Y_VALUE_RANGE Y_VALUE_RANGE
                        Set y-limit according to your assigned bound value.
```

### 示例

#### Windows

**警告**：以下代码应直接复制到终端运行。直接运行对应的 _example.ps1_ 脚本可能会因为 UTF8 字符而报错（除非启用了系统 UTF8 支持）

```powershell
python3 BL_420N_visualisation.py `
    --no-gui `
    -i 'test_data/Data1_2024_11_13 ch1.txt' 'test_data/Data2_2024_11_13 ch2.txt' `
    -o 'test_data/saved_figure.png' `
    -s '00:12:59' `
    -e '00:14:45' `
    -t 'This is the Title' `
    --xlabel 'assign x-label' `
    --ylabel 'assign y-label' `
    --label 'label of Artery_Pressure' 'label of CVP' `
    --add-incident 00:13:30 'Something Occurred!' 00:14:03 '另一件事！' `
    --y-quantile-range 0.1 0.9
```

- `-i` 指定单通道原始文件
- `-o` 指定图片的保存路径
- `-s` 指定数据可视化的开始时间，必须满足 `HH:MM:SS` 或 `MM:SS` 的格式要求
- `-e` 指定数据可视化的结束时间，必须满足 `HH:MM:SS` 或 `MM:SS` 的格式要求
- `-t` 指定图例的标题
- `--xlabel` 指定 x 轴坐标名称
- `--ylabel` 指定 y 轴坐标名称
- `--label` 指定 `-i` 对应每条直线的折线图的“标签”(label)

#### Linux

近几年的 Linux 发行版默认支持 UTF-8，可以直接运行 `example.sh`。

```bash
python3 BL_420N_visualisation.py \
    --no-gui \
    -i 'test_data/Data1_2024_11_13 ch1.txt' 'test_data/Data2_2024_11_13 ch2.txt' \
    -o 'test_data/saved_figure.png' \
    -s '00:12:59' \
    -e '00:14:45' \
    -t 'This is the Title' \
    --xlabel 'assign x-label' \
    --ylabel 'assign y-label' \
    --label 'label of Artery_Pressure' 'label of CVP' \
    --add-incident 00:13:30 'Something Occurred!' 00:14:03 '另一件事！' \
    --y-quantile-range 0.1 0.9
```

## 环境创建

### Windows

```powershell
python -m venv .venv
.venv\Scripts\Activate.ps1
pip install -r requirements.txt
```

你可能需要手动调整脚本运行策略以运行脚本。

以终端 / powershell 管理员模式运行

```powershell
Set-ExecutionPolicy -Scope CurrentUser bypass
```

以授予运行脚本的权限。

当然，你也可以直接运行 `pip install -r requirements.txt` 全局安装相关库。

### Linux

```bash
python3 -m venv .venv
.venv/bin/activate
pip install -r requirements.txt
```

## 打包

在已配置好的环境下运行 _build.py_

若无额外需求，运行 `python nuitka` 或 `python pyinstaller` 即可。打包程序将自动输出到 _./dist_ 目录下

```text
usage: build.py [-h] [--generate {FILE,DIR,COMPRESSDIR}] [--output OUTPUT]
                {nuitka,pyinstaller} ...

positional arguments:
  {nuitka,pyinstaller}  Valid subcommands
    nuitka              Build with nuitka
    pyinstaller         Build with PyInstaller

options:
  -h, --help            show this help message and exit
  --generate {FILE,DIR,COMPRESSDIR}, -g {FILE,DIR,COMPRESSDIR}
                        Generate one executable or a folder containing an
                        executable and other components. The folder can be
                        compressed. [Default FILE]
  --output OUTPUT, -o OUTPUT
                        Output directory
------------------------------------------------------------------------------
usage: build.py nuitka [-h] [--compiler {GCC,CLANG,MSVC,MINGW64}]
                       [--lto {AUTO,ON,OFF}]

options:
  -h, --help            show this help message and exit
  --compiler {GCC,CLANG,MSVC,MINGW64}
                        Choose compiler from MSVC, Clang and GCC. Default Auto
                        (let nuitka choose).
  --lto {AUTO,ON,OFF}   Whether or not perform LTO
------------------------------------------------------------------------------
usage: build.py pyinstaller [-h] [--optimize {OFF,THIN,FULL}]

options:
  -h, --help            show this help message and exit
  --optimize {OFF,THIN,FULL}, --opt {OFF,THIN,FULL}
                        OFF: No optimization. THIN: Remove assert sentence but
                        keep documentation. FULL: Remove assert sentence and
                        documentation string. [Default THIN]
```

## TODO

- [X] 重构文件读取函数，使用 polars 原生函数，大幅提升文件读取速度（预估提升 100 倍)
- [X] 添加从原始文件中提取测量指标并传递到 label 的功能
- [X] 基于 QT 的 GUI 客户端
- [ ] 打包脚本自动根据编译选项和程序版本对输出文件进行命名

## 特别致谢

感谢 RIBBON 提供的文件读取方法，可以大幅度提高文件读取速度。
