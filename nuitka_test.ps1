./plot_data.exe `
    -i 'test_data/Data1_2024_11_13 ch1.txt' 'test_data/Data2_2024_11_13 ch2.txt' `
    -o 'test_data/saved_figure.png' `
    -s '00:12:59' `
    -e '00:14:45' `
    -t 'This is the Title' `
    --xlabel 'assign x-label' `
    --ylabel 'assign y-label' `
    --label 'label of Artery_Pressure' 'label of CVP' `
    --add-incident 00:13:30 'Something Occurred!' 00:14:03 '其他事情！' `
    --y-quantile-range 0.1 0.9
